Angle of rot. about x-axis (Floor 1): 0
Angle of rot. about y-axis (Floor 1): 0
Angle of rot. about z-axis (Floor 1): 0
Translation Matrix of Floor 1
[[2040.]
 [2063.]
 [   0.]]
Rotation Matrix of Floor 1
[[1 0 0]
 [0 1 0]
 [0 0 1]]
Angle of rot. about x-axis (Floor 2): 0
Angle of rot. about y-axis (Floor 2): 0
Angle of rot. about z-axis (Floor 2): [-1.78598145]
Translation Matrix of Floor 2
[[  805.30486334]
 [-2319.44908913]
 [    0.        ]]
Rotation Matrix of Floor 2
[[-0.21352828  0.97693688  0.        ]
 [-0.97693688 -0.21352828  0.        ]
 [ 0.          0.          1.        ]]
Angle of rot. about x-axis (Floor 3): 0
Angle of rot. about y-axis (Floor 3): 0
Angle of rot. about z-axis (Floor 3): [0.08740137]
Translation Matrix of Floor 3
[[ 627.78097202]
 [1473.42324238]
 [   0.        ]]
Rotation Matrix of Floor 3
[[ 0.99618293 -0.08729013  0.        ]
 [ 0.08729013  0.99618293  0.        ]
 [ 0.          0.          1.        ]]